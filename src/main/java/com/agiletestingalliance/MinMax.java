package com.agiletestingalliance;

public class MinMax {

    public int findMax(int operandA, int operandB) {
        if (operandB > operandA){
            return operandB;
        }
	else{
            return operandA;
	}
    }

}
