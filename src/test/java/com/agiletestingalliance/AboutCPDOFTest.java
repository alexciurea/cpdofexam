package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;
import com.agiletestingalliance.AboutCPDOF;

public class AboutCPDOFTest {
    @Test
    public void testDesc() throws Exception {

        String kompare = new AboutCPDOF().desc();
        assertTrue("desc value", kompare.contains("Tools Driven"));

    }
}

