package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;
import com.agiletestingalliance.MinMax;

public class MinMaxTest {
    @Test
    public void testFindMax() throws Exception {

        int kompare = new MinMax().findMax(7,5);
        assertEquals("Max", 7, kompare);

    }
    @Test
    public void testFindMin() throws Exception {

        int kompare = new MinMax().findMax(5,10);
        assertEquals("Min", 10, kompare);

    }

}

